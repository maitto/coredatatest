//
//  InputViewController.swift
//  HitList
//
//  Created by Mortti Aittokoski on 19.4.2016.
//  Copyright © 2016 Mortti Aittokoski. All rights reserved.
//

import UIKit
import CoreData

class InputViewController: UIViewController, UITextFieldDelegate{
    
    //var aViewControllerInstance = ViewController()
    
    
    @IBOutlet weak var textOutlet: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textOutlet.delegate = self
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("return painettu")
        textField.resignFirstResponder()
        print("done editing")
        ViewController.sharedInstance.saveName(textOutlet.text!)
        ViewController.sharedInstance.postStuff(textOutlet.text!)
        
        return true
    }
    
    
   /* @IBAction func textFinishedAction(sender: AnyObject) {
        print("done editing")
        classi.saveName(textOutlet.text!)
    }*/
    
}